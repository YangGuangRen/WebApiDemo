﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models.Request
{
    /// <summary>
    /// 注册信息
    /// </summary>
    public class RegisterUserRequest
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string Phone { get; set; }
        /// <summary>
        /// 手机验证码
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Required]
        public string CompanyName { get; set; }
        /// <summary>
        /// 类型（1 反派  2 正派）
        /// </summary>
        [Required]
        public int Type { get; set; }
    }
}