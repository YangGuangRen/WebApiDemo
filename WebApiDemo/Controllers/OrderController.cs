﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDemo.Models.Request;
using WebApiDemo.Models.Response;

namespace WebApiDemo.Controllers
{
    /// <summary>
    /// 订单管理模块
    /// </summary>
    [RoutePrefix("v1")]
    public class OrderController : ApiController
    {
        /// <summary>
        /// 获取订单列表
        /// </summary>
        [Route("order/list")]
        [HttpGet]
        public OrderListGetResponse GetOrderList([FromUri]OrderListGetRequest request)
        {
            if (request.OrderId <= 0) { throw new CustomException("请输入正确的订单号"); }

            var list = new List<OrderModel>(){
                new OrderModel() {  OrderId=1001, UserName="绿巨人", OrderDate=DateTime.Now.AddDays(-1)},
                new OrderModel() {  OrderId=1002, UserName="钢铁侠", OrderDate=DateTime.Now.AddDays(-2)},
                new OrderModel() {  OrderId=1003, UserName="哨兵", OrderDate=DateTime.Now.AddDays(-3)},
                new OrderModel() {  OrderId=1004, UserName="灭霸", OrderDate=DateTime.Now.AddDays(-4)},
                new OrderModel() {  OrderId=1005, UserName="毒液", OrderDate=DateTime.Now.AddDays(-5)},
                new OrderModel() {  OrderId=1006, UserName="超人", OrderDate=DateTime.Now.AddDays(-6)},
            };
            return new OrderListGetResponse()
            {
                List = list,
                total = 20
            };
        }
        /// <summary>
        /// 获取订单详细信息
        /// </summary>
        /// <param name="orderId">订单号</param>
        [Route("order/info")]
        [HttpGet]
        public OrderInfoGetResponse GetOrderInfo(int orderId)
        {
            return new OrderInfoGetResponse()
            {
                OrderId = orderId,
                UserName = "绿巨人",
                Remark = "您输入了：" + orderId.ToString()
            };
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="orderId">订单号</param>
        [Route("order/cancel")]
        [HttpPost]
        public bool CancelOrder(int orderId)
        {
            return true;
        }


    }
}
